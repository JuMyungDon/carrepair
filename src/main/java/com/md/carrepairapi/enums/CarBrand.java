package com.md.carrepairapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarBrand {
    HUNDAI("현대")
    ,KIA("기아")
    ,BMW("BMW")
    ,DOYOTA("도요타")
    ;

    private final String name;
}

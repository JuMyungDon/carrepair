package com.md.carrepairapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PartName {
    TIRE("타이어", false)
    ,WIPER("와이퍼", false)
    ,REARVIEW_MIRROR("백미러", false)
    ,ENGINE_OIL("엔진오일", true)
    ;

    private final String name;
    private final Boolean isCheck;
}

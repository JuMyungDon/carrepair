package com.md.carrepairapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarRepairApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarRepairApiApplication.class, args);
    }

}

package com.md.carrepairapi.service;

import com.md.carrepairapi.entity.Car;
import com.md.carrepairapi.model.CarItem;
import com.md.carrepairapi.model.CarRequest;
import com.md.carrepairapi.model.CarResponse;
import com.md.carrepairapi.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarService {
    private final CarRepository carRepository;

    public void setCar(CarRequest request) {
        Car addData = new Car();
        addData.setCarBrand(request.getCarBrand());
        addData.setPartName(request.getPartName());
        addData.setRepairDate(request.getRepairDate());
        addData.setRepairDetail(request.getRepairDetail());
        addData.setPrice(request.getPrice());
        addData.setMemo(request.getMemo());

        carRepository.save(addData);
    }

    public List<CarItem> getCars() {
        List<Car> originList = carRepository.findAll();

        List<CarItem> result = new LinkedList<>();

        for (Car car : originList) {
            CarItem addItem = new CarItem();
            addItem.setId(car.getId());
            addItem.setCarBrand(car.getCarBrand().getName());
            addItem.setPartName(car.getPartName().getName());
            addItem.setIsCheck(car.getPartName().getIsCheck() ? "예" : "아니오");
            addItem.setRepairDate(car.getRepairDate());
            addItem.setRepairDetail(car.getRepairDetail());
            addItem.setPrice(car.getPrice());

            result.add(addItem);
        }

        return result;
    }

    public CarResponse getCar(long id) {
        Car originData = carRepository.findById(id).orElseThrow();

        CarResponse response = new CarResponse();
        response.setCarBrand(originData.getCarBrand().getName());
        response.setPartName(originData.getPartName().getName());
        response.setIsCheck(originData.getPartName().getIsCheck() ? "예" : "아니오");
        response.setRepairDate(originData.getRepairDate());
        response.setRepairDetail(originData.getRepairDetail());
        response.setPrice(originData.getPrice());

        return response;
    }
}

package com.md.carrepairapi.entity;

import com.md.carrepairapi.enums.CarBrand;
import com.md.carrepairapi.enums.PartName;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private CarBrand carBrand;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PartName partName;

    @Column(nullable = false)
    private LocalDate repairDate;

    @Column(nullable = false, length = 30)
    private String repairDetail;

    @Column(nullable = false)
    private Double price;

    @Column(columnDefinition = "TEXT")
    private String memo;
}

package com.md.carrepairapi.controller;

import com.md.carrepairapi.model.CarItem;
import com.md.carrepairapi.model.CarRequest;
import com.md.carrepairapi.model.CarResponse;
import com.md.carrepairapi.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/car")
public class CarController {
    private final CarService carService;

    @PostMapping("/repair")
    public String setCar(@RequestBody CarRequest request) {
        carService.setCar(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<CarItem> setCars() {
        return carService.getCars();
    }

    @GetMapping("/detail/{id}")
    public CarResponse setCar(@PathVariable long id) {
        return carService.getCar(id);
    }
}

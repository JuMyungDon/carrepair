package com.md.carrepairapi.model;

import com.md.carrepairapi.enums.CarBrand;
import com.md.carrepairapi.enums.PartName;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarRequest {
    @Enumerated(value = EnumType.STRING)
    private CarBrand carBrand;
    @Enumerated(value = EnumType.STRING)
    private PartName partName;
    private LocalDate repairDate;
    private String repairDetail;
    private Double price;
    private String memo;
}

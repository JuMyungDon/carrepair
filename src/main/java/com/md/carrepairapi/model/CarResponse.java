package com.md.carrepairapi.model;

import com.md.carrepairapi.enums.CarBrand;
import com.md.carrepairapi.enums.PartName;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarResponse {
    private String carBrand;
    private String partName;
    private String isCheck;
    private LocalDate repairDate;
    private String repairDetail;
    private Double price;
}
